import React from 'react';
import './AppBody.css';
import Logo from '../Logo/Logo';
import Chat from '../Chat/Chat';

class AppBody extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className="AppBody">
        <Logo height="48px"/>
        <Chat />
      </div>
    );
  }

}

export default AppBody;
