import React from 'react';

import { connect } from 'react-redux';

import './LoginPage.css';

import ChatHeader from '../ChatHeader/ChatHeader';
import ChatBody from '../ChatBody/ChatBody';
import ChatInput from '../ChatInput/ChatInput';
import ChatMessage from '../ChatMessage/ChatMessage';

class LoginPage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return(
      <div className="Chat">
        <div>
          <div>
            <ChatHeader />
            <ChatBody />
            <ChatInput />
          </div>
        </div>
      </div>
    );

  }

}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
