import React from 'react';

import './Chat.css';

import ChatHeader from '../ChatHeader/ChatHeader';
import ChatBody from '../ChatBody/ChatBody';
import ChatInput from '../ChatInput/ChatInput';
import ChatMessage from '../ChatMessage/ChatMessage';
import loadData from '../DataService/DataService';

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.addMessage = this.addMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
  }

  lastId = 0;

  static beautyTime(time) {
    if (time == '') {
      return time;
    }
    return time.slice(11, 13) + ':' + time.slice(14, 16) + ':' + time.slice(17, 19) + ' '
         + time.slice(0, 4) + '.' + (Number(time.slice(5, 7)) - 1) + '.' + time.slice(8, 10);
  }

  componentDidMount() {
    loadData('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then(response => {
        return JSON.parse(response);
      })
      .then((response) => {
        let i = 0;
        response = response.map(function(message) {
          message['createdAt'] = Chat.beautyTime(message['createdAt']);
          message['editedAt'] = Chat.beautyTime(message['editedAt']);
          message['id'] = String(i++);
          return message;
        })
        this.lastId = i;
        this.setState({messages: response});
      });
  }

  addMessage(message) {
    let temp = this.state.messages;
    message.id = this.lastId++;
    temp.push(message);
    this.setState({ messages: temp });
  }

  deleteMessage(messageId) {
    let temp = this.state.messages;
    temp = temp.filter(message => message.id != messageId);
    // console.log(temp.length);
    // console.log(this.state.messages.length);
    // console.log(messageId);
    this.setState({ messages: temp });
  }

  render() {

    const data = this.state;

    return(
      <div className="Chat">
        <div>
          <div>
            <ChatHeader />
            {data != null && <ChatBody messages={data.messages} deleteMessage={this.deleteMessage}/>}
            <ChatInput addMessage={this.addMessage}/>
          </div>
        </div>
      </div>
    );

  }

}

export default Chat;
