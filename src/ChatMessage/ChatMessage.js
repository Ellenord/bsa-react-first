import React from 'react';
import './ChatMessage.css';
import like from '../img/like.png';
import dislike from '../img/notlike.svg';

class ChatMessage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id: props['message']['id'],
      userId: props['message']['userId'],
      avatar: props['message']['avatar'],
      user: props['message']['user'],
      text: props['message']['text'],
      createdAt: props['message']['createdAt'],
      editedAt: props['message']['editedAt']
    };
    this.deleteMessage = this.deleteMessage.bind(this);
  }

  deleteMessage(e) {
    this.props.deleteMessage(this.state.id);
  }

  render() {

    const data = this.state;

    return(
      <div className={'FlexWrapper' + (data['userId'] == '01234567-8901-2345-6789-012356789012' ? ' SelfMessage' : '')}>
        <div className='ChatMessage'>
          <div className="ChatMessageContent">
            <img src={data.avatar} />
            <p>
            <span>{data.user}</span><br/>
            {data.text}
            </p>
          </div>
          <div className="ChatMessageController">
            <i>{data.createdAt}</i>
            <label for={data.id}>
              <img className="ChatMessageLike" src={like} />
              <input type="checkbox" id={data.id} />
              <img className="ChatMessageDislike" src={dislike} />
            </label>
          </div>
          {data['userId'] == '01234567-8901-2345-6789-012356789012' &&
          <div className="SelfMessageController">
            <button onClick={this.deleteMessage}>Delete</button>
          </div>
          }
        </div>
        <div className="FreeSpace"></div>
      </div>
    );
  }

}

export default ChatMessage;
