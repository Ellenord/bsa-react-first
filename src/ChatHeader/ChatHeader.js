import React from 'react';
import './ChatHeader.css';

class ChatHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      chatName: 'TopSecret',
      participantsCount: 420,
      messageCount: 228,
      lastMessageTime: '12:34'
    }
  }

  render() {
    const data = this.state;
    return (
      <div className="ChatHeader">
        <div className="ChatHeaderContent">
          <div className="ChatHeaderLeft">
            <div className="ChatName">
              <strong>{data.chatName}</strong>
            </div>
            <div className="ParticipantsCount">
              <strong>{data.participantsCount} participants</strong>
            </div>
            <div className="MessageCount">
              <strong>{data.messageCount} messages</strong>
            </div>
          </div>
          <div className="ChatHeaderRight">
            <i>Last message at: {data.lastMessageTime}</i>
          </div>
        </div>
      </div>
    );
  }

}

export default ChatHeader;
