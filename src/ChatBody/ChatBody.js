import React from 'react';
import ReactDOM from 'react-dom';
import './ChatBody.css';

import ChatMessage from '../ChatMessage/ChatMessage';

class ChatBody extends React.Component {

  constructor(props) {
    super(props);
  }

  static messagesDateComparator(a, b) {
    a = a['createdAt'];
    b = b['createdAt'];
    if (Number(a.slice(9, 13)) != Number(b.slice(9, 13))) {
      return Number(a.slice(9, 13)) - Number(b.slice(9, 13));
    } else if (Number(a.slice(14, 16)) != Number(b.slice(14, 16))) {
      return Number(a.slice(14, 16)) - Number(b.slice(14, 16));
    } else if (Number(a.slice(17, 19)) != Number(b.slice(17, 19))) {
      return Number(a.slice(17, 19)) - Number(b.slice(17, 19));
    } else if (Number(a.slice(0, 2) != Number(b.slice(0, 2)))) {
      return Number(a.slice(0, 2)) - Number(b.slice(0, 2));
    } else if (Number(a.slice(3, 5)) != Number(b.slice(3, 5))) {
      return Number(a.slice(3, 5)) - Number(b.slice(3, 5));
    } else if (Number(a.slice(6, 8)) != Number(b.slice(6, 8))) {
      return Number(a.slice(6, 8)) - Number(b.slice(6, 8));
    }
  }

  render() {

    return (
      <div id="ChatBody" className="ChatBody">
        {this.props.messages.sort(ChatBody.messagesDateComparator).map((message: Object) => {
            return <ChatMessage message={message} deleteMessage={this.props.deleteMessage}/>;
          })}
      </div>
    );

  }

}

export default ChatBody;
