import React from 'react';
import './Logo.css';
import logo from './../img/logo.svg'

class Logo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      height: props.height
    };
  }

  render() {
    return(
      <div className="Logo">
        <img height={this.state.height} src={logo} alt='logo' />
      </div>
    );
  }

}

export default Logo;
